import webapp

form = """
    <form action="" method="POST">
        <p>Content: <input type="text" name="content"></p>
        <p><input type="submit" value="Submit"></p>
    </form>
"""

page_found = """
<html>
    <body>
        <h1>Requested resource: {resource}</h1>
            <p>Resource: {resource}</p>
            <p>Content: {content}</p>
        <h1>You can update the content with this form</h1>
            <p>{form}</p>
    </body>
</html>
"""

page_not_found = """
<html>
    <body>
        <h1>Requested resource {resource} not found</h1>
        <h1>You can add this resource with this form</h1>
            <p>{form}</p>
    </body>
</html>
"""


class ContentPostApp(webapp.webApp):
    def __init__(self, hostname, port):
        self.contents = {}
        super().__init__(hostname, port)

    def parse(self, request):
        data = {'method': request.split(' ', 2)[0],
                'resource': request.split(' ', 2)[1],
                'body': request.split('=')[-1]}

        return data

    def process(self, data):
        if data['method'] == 'GET':
            http_code, http_page = self.get(data['resource'])
        elif data['method'] == 'POST':
            http_code, http_page = self.post(data['resource'], data['body'])

        return http_code, http_page

    def get(self, resource):
        if resource in self.contents.keys():
            content = self.contents[resource]
            http_page = page_found.format(resource=resource, content=content, form=form)
            http_code = '200 OK'
        else:
            http_page = page_not_found.format(resource=resource, form=form)
            http_code = '404 Not Found'

        return http_code, http_page

    def post(self, resource, body):
        self.contents[resource] = body
        http_page = page_found.format(resource=resource, content=body, form=form)
        http_code = '200 OK'

        return http_code, http_page


if __name__ == "__main__":
    testContentPostApp = ContentPostApp("", 1234)
